# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 15:01:38 2019
@title: Imputation error for algorithms
Description: PLots figures for Sections 2.4, 2.5 of report
@author: Pablo
"""

import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import numpy as np
import cvxpy_ex as cvx
import softimpute as si
import gaussian_creation as gc
import SVT_algorithm_correct as sv


n = 20 #n is number of different sampling probabilities
x = np.linspace(0.01, 0.75, n)

si_time_list = []
si_time = []
si_time_std = []
si_error_list = []
si_error = []
si_error_std = []

svt_time_list = []
svt_time = []
svt_time_std = []
svt_error_list = []
svt_error = []
svt_error_std = []
svt_time = []
svt_error = []

cvx_time_list = []
cvx_time = []
cvx_time_std = []
cvx_error_list = []
cvx_error = []
cvx_error_std = []
cvx_time = []
cvx_error = []

for j in range(n):
    print(j)
    si_time_list = []
    si_error_list = []
    svt_time_list = []
    svt_error_list = []
    cvx_time_list = []
    cvx_error_list = []
    for i in range(10):
        np.random.seed(i+15) #Set seed so same matrices are analysed for each p
        UV, Z, sigma_e = gc.noisy_gaussian(5, 50, 50, SNR=2)
        P_om_Z, mask, indices, values = gc.gaussian_remove_entries(Z, x[j])
        print(i)
        
        _, time, error =  si.soft_impute(P_om_Z, mask, UV)
        si_time_list.append(time)
        si_error_list.append(error)
        
        _, time, error =  sv.SVT(P_om_Z, mask, UV, x[j])
        svt_time_list.append(time)
        svt_error_list.append(error)
        
        time, error =  cvx.cvxp(P_om_Z, mask, UV, x[j], sigma_e, indices, values)
        cvx_time_list.append(time)
        cvx_error_list.append(error)
        
    si_avg_time = np.mean(si_time_list)
    si_avg_time_std = np.std(si_time_list)
    si_avg_error = np.mean(si_error_list)
    si_avg_error_std = np.std(si_error_list)
    si_time.append(si_avg_time)
    si_error.append(si_avg_error)
    si_time_std.append(si_avg_time_std)
    si_error_std.append(si_avg_error_std)
    
    svt_avg_time = np.mean(svt_time_list)
    svt_avg_time_std = np.std(svt_time_list)
    svt_avg_error = np.mean(svt_error_list)
    svt_avg_error_std = np.std(svt_error_list)
    svt_time.append(svt_avg_time)
    svt_error.append(svt_avg_error)
    svt_time_std.append(svt_avg_time_std)
    svt_error_std.append(svt_avg_error_std)
    
    cvx_avg_time = np.mean(cvx_time_list)
    cvx_avg_time_std = np.std(cvx_time_list)
    cvx_avg_error = np.mean(cvx_error_list)
    cvx_avg_error_std = np.std(cvx_error_list)
    cvx_time.append(cvx_avg_time)
    cvx_error.append(cvx_avg_error)
    cvx_time_std.append(cvx_avg_time_std)
    cvx_error_std.append(cvx_avg_error_std)
    
    
    

plt.figure()
plt.errorbar(x, si_error, yerr = si_error_std, fmt='-o', markersize=4, capsize=4,color = 'blue', label = 'Soft-Impute');
plt.errorbar(x, svt_error, yerr = svt_error_std, fmt='-o', markersize=4, capsize=4,color = 'red', label = 'SVT'); 
plt.errorbar(x, cvx_error, yerr = cvx_error_std, fmt='-o', markersize=4, capsize=4,color = 'gray', label = 'cvxpy');   
#plt.plot(x, cvx_error, color = 'red' , label = 'CVXPY');
#plt.plot(x, svt_error, color = 'green', label = 'SVT');

plt.figure()
plt.errorbar(x, si_time,yerr = si_time_std, fmt='-o', markersize=4, capsize=4, color = 'blue', label = 'Soft-Impute');    
plt.errorbar(x, svt_time,yerr = svt_time_std, fmt='-o', markersize=4, capsize=4, color = 'red', label = 'SVT'); 
plt.errorbar(x, cvx_time, yerr = cvx_time_std, fmt='-o', markersize=4, capsize=4,color = 'gray', label = 'cvxpy');  
#plt.plot(x, cvx_time, color = 'red' , label = 'CVXPY');
#plt.plot(x, svt_time, color = 'green', label = 'SVT');


