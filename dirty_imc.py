# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27
dirtyIMC Algorithm : Matrix Completion with noisy side information
@author: Pablo
"""

import numpy as np
import time



def dirtyimc(R, mask, UV, A, B):
    """Performs dirtyIMC algorithm on input matrix on input matrix R 
    with side information matrices A and B.
    Outputs error with respect to full matrix UV"""
    
    t_start = time.time()
    
    error_Z_array = []
    error_pct = 0
    avg_time = 0
    
    r_a , r_b = len(A[0]), len(B[0])
    m, n = len(R), len(R[0])
    
   
    
    Z_old_M = np.zeros((r_a,r_b))
    Z_old_N = np.zeros((m,n))
    Z_hat = np.zeros((m,n))
    #print(Z_old)
    
    
    error_Z = 10
    
    #Determine parameter fact (or alpha in report)
    p_entries = 1 - len(np.nonzero(R)[0])/(m*n)
    if p_entries<0.2:
        fact = 0.05
    elif p_entries<0.75:
        fact = (p_entries)**(3/2)
    else:
        fact = 1
        
    lam_m = fact*(np.linalg.norm(A, 2)+np.linalg.norm(B, 2))/3 #Largest singular value of matrix
    #print(lam_m)
    lam_n = lam_m
    l = 0
    
    A_pinv = np.linalg.pinv(A)
    B_pinv = np.linalg.pinv(B)
    
    prod_1 = np.matmul(np.matmul(A, Z_old_M),np.transpose(B))
    
    
    for l in range(2000):
        #print(l)
        
        """-------Update M, N fixed---------"""
        update_mat = np.matmul(A_pinv,np.matmul(mask*((R - Z_old_N) - prod_1), np.transpose(B_pinv)))

        
        Y = Z_old_M + update_mat
        U,D,VT=(np.linalg.svd(Y, full_matrices=False))
        
        D_thresh = np.maximum(D - lam_m, 0)
        #print(D_thresh)    
    
        Sigma = np.diag(D_thresh)
            # reconstruct matrix
        Z_hat_M = np.linalg.multi_dot([U, Sigma, VT])
        
        Z_old_M = Z_hat_M
        prod_1 = np.matmul(np.matmul(A, Z_old_M),np.transpose(B))
        
        
        """-------Update N, M fixed---------"""
        update_mat = mask*((R - prod_1) - Z_old_N)
        #print(update_mat)
        
        Y = Z_old_N + update_mat
        U,D,VT=(np.linalg.svd(Y, full_matrices=False))
        
        D_thresh = np.maximum(D - lam_n, 0)    
    
        Sigma = np.diag(D_thresh)
            # reconstruct matrix
        Z_hat_N = np.linalg.multi_dot([U, Sigma, VT])
        
        Z_old_N = Z_hat_N
        Z_old = Z_hat
        Z_hat = Z_hat_N + prod_1
        
        stopping_crit = (np.linalg.norm(Z_hat - Z_old, "fro")/np.linalg.norm(Z_hat, "fro"))
        if stopping_crit < 1e-6:
            break
        
        
        #lam_n = lam_n*((l+1)/(l+2))
        #lam_m = lam_m*((l+1)/(l+2))
        
        
    if Z_hat is not None:
        error_pct += (np.linalg.norm(Z_hat - UV, "fro")/np.linalg.norm(UV, "fro"))
        t_end = time.time()
        avg_time += t_end -t_start
    

    return Z_hat, Z_hat_N, Z_hat_M, avg_time, error_pct

