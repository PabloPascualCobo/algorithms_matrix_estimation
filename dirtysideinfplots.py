# -*- coding: utf-8 -*-
"""
Created on Sun Mar 01 20:56:01 2020
Plots for Performance of Dirty Side Information Matrices - section 3.4 of report
@author: Pablo
"""
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import numpy as np
#import cvxpy_ex as cvx
import softimpute as si
import gaussian_creation as gc
import SVT_algorithm_correct as sv
import imc
import sideinf as sid
import noise_features as nf
import dirty_imc


n = 20 #n is number of different sampling probabilities
x = np.linspace(0, 0.95, n)

si_time_list = []
si_time = []
si_time_std = []
si_error_list = []
si_error = []
si_error_std = []

svt_time_list = []
svt_time = []
svt_time_std = []
svt_error_list = []
svt_error = []
svt_error_std = []
svt_time = []
svt_error = []

sid_time_list = []
sid_time = []
sid_time_std = []
sid_error_list = []
sid_error = []
sid_error_std = []

dsid_time_list = []
dsid_time = []
dsid_time_std = []
dsid_error_list = []
dsid_error = []
dsid_error_std = []


for j in range(n):
    print(j)
    si_time_list = []
    si_error_list = []
    svt_time_list = []
    svt_error_list = []
    sid_time_list = []
    sid_error_list = []
    dsid_time_list = []
    dsid_error_list = []
    for i in range(10):
        np.random.seed(i) #Set seed so same matrices are analysed for each p
        A,_,B,M = sid.sideinf_mat_nonorth(100, 100, 12, 8, 5)
        A_new = nf.noisy_feature(A, x[j])
        B_new = nf.noisy_feature(B, x[j])

        M_new, mask, _, _ = gc.gaussian_remove_entries(M, 0.8)

        print(i)
        
        _, _, _, time, error =  dirty_imc.dirtyimc(M_new, mask, M, A_new, B_new)
        print(error)
        dsid_time_list.append(time)
        dsid_error_list.append(error)
        
        _, time, error =  imc.sideinf_imc(M_new, mask, M, A_new, B_new)
        sid_time_list.append(time)
        sid_error_list.append(error)
        
        
        _, time, error =  si.soft_impute(M_new, mask, M)
        print(error)
        si_time_list.append(time)
        si_error_list.append(error)
        
        
    si_avg_time = np.mean(si_time_list)
    si_avg_time_std = np.std(si_time_list)
    si_avg_error = np.mean(si_error_list)
    si_avg_error_std = np.std(si_error_list)
    si_time.append(si_avg_time)
    si_error.append(si_avg_error)
    si_time_std.append(si_avg_time_std)
    si_error_std.append(si_avg_error_std)
    
    
    sid_avg_time = np.mean(sid_time_list)
    sid_avg_time_std = np.std(sid_time_list)
    sid_avg_error = np.mean(sid_error_list)
    sid_avg_error_std = np.std(sid_error_list)
    sid_time.append(sid_avg_time)
    sid_error.append(sid_avg_error)
    sid_time_std.append(sid_avg_time_std)
    sid_error_std.append(sid_avg_error_std)
        
    dsid_avg_time = np.mean(dsid_time_list)
    dsid_avg_time_std = np.std(dsid_time_list)
    dsid_avg_error = np.mean(dsid_error_list)
    dsid_avg_error_std = np.std(dsid_error_list)
    dsid_time.append(dsid_avg_time)
    dsid_error.append(dsid_avg_error)
    dsid_time_std.append(dsid_avg_time_std)
    dsid_error_std.append(dsid_avg_error_std)
    

plt.figure()
plt.rcParams.update({'font.size':12})
plt.errorbar(x, si_error, yerr = si_error_std, fmt='-o', markersize=4, capsize=4,color = 'blue', label = 'Soft-Impute');
plt.errorbar(x, sid_error, yerr = sid_error_std, fmt='-o', markersize=4, capsize=4,color = 'black', label = 'IMC');
plt.errorbar(x, dsid_error, yerr = dsid_error_std, fmt='-o', markersize=4, capsize=4,color = 'green', label = 'Dirty IMC');

plt.figure()
plt.errorbar(x, si_time,yerr = si_time_std, fmt='-o', markersize=4, capsize=4, color = 'blue', label = 'Soft-Impute');    
plt.errorbar(x, sid_time,yerr = sid_time_std, fmt='-o', markersize=4, capsize=4, color = 'black', label = 'IMC');
plt.errorbar(x, dsid_time,yerr = dsid_time_std, fmt='-o', markersize=4, capsize=4, color = 'green', label = 'Dirty IMC');
