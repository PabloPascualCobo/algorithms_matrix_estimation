# -*- coding: utf-8 -*-
"""
Created on Mon Dec 23 12:08:31 2019
@Title: Applying Matrix Completion on MovieLens data
@author: Pablo
Description: Use matrix completion algorithms to predict movie ratings 
from MovieLens 1M dataset, Section 2.6 of report.
Reference websites: 
https://medium.com/analytics-vidhya/implementation-of-a-mov
ies-recommender-from-implicit-feedback-6a810de173ac

https://beckernick.github.io/matrix-factorization-recommender/
"""
import pandas as pd
import numpy as np
import SVT_algorithm_correct as sv
import softimpute as si
import gaussian_creation as gc
import matplotlib.pyplot as plt

def load_data():
    #Creates a 3-column user, movie, rating table
    '''load the MovieLens 1m dataset in a Pandas dataframe'''
    ratings = pd.read_csv('ml-1m/ratings.dat', delimiter='::', header=None, 
        names=['user_id', 'movie_id', 'rating', 'timestamp'], 
        usecols=['user_id', 'movie_id', 'rating'], engine='python')

    return ratings

"""Pre-processing"""
df = load_data()

R_df = df.pivot(index = 'user_id', columns ='movie_id', values = 'rating')

R = np.array(R_df)


prob = [0.1, 0.3, 0.5, 0.7]
t = 500 #Subsampling parameter

error_si = []
error_svt = []
std_si = []
std_svt = []

for i in prob:
    rmse_si_array = []
    rmse_svt_array = []
    p = i
    for j in range(10):
        print(j)
        np.random.seed(10+j)
        """Randomly subsample matrices- columns first then rows"""
        A = R[:,np.random.choice(R.shape[1], t, replace=False)]
        A = A[np.random.choice(A.shape[0], t, replace=False),:]
        A = A[:,:-1]
        A_base = np.nan_to_num(A)
        A0=np.nan_to_num(A)
        mask0 = np.nan_to_num(A0/A0)
        
        
        """Remove proportion p of entries"""
        print(p)
        A, mask1,_,_ = gc.gaussian_remove_entries(A0,p)
        mask2 = mask0*(1 - mask1) #Gives 1 in position of removed, known entries
        mask = mask0*mask1 #Remaining, known entries
        test_size = np.sum(mask2)
        real_p = test_size/(np.sum(mask0))
        
        """Calculate mean - done here as can only use available entries"""
        A[A==0]=np.nan
        mean_user_values = np.nan_to_num(np.array(np.nanmean(A, axis=1)).reshape(t,1))
        A = np.nan_to_num(A - mean_user_values)
        A0 = np.nan_to_num(A0 -mean_user_values)
        
        
        """Run matrix completion algorithm"""
        """SVT"""
        B, time_run, _ = sv.SVT(A, mask, A0, p)
        
        """Post-processing - add back mean"""
        B = np.matrix.round(B + mean_user_values)

        """Thresholds to ensure all predictions lie between 1 and 5"""
        B[B>5]=5
        B[B<1]=1

        
        rmse_svt = (np.linalg.norm(mask2*(B - A_base), "fro")/np.linalg.norm(mask2, "fro"))
        print(rmse_svt)
        rmse_svt_array.append(rmse_svt)
        
        """SOFT-IMPUTE"""
        B, time_run, _ = si.soft_impute(A, mask, A0)

        
        """Post-processing - add back mean"""
        B = np.matrix.round(B + mean_user_values)

        """Thresholds to ensure all predictions lie between 1 and 5"""
        B[B>5]=5
        B[B<1]=1
        
        rmse_si = (np.linalg.norm(mask2*(B - A_base), "fro")/np.linalg.norm(mask2, "fro"))
        print(rmse_si)
        rmse_si_array.append(rmse_si)
    rmse_svt_p_mean = np.mean(rmse_svt_array)
    rmse_svt_p_std = np.std(rmse_svt_array)
    rmse_si_p_mean = np.mean(rmse_si_array)
    rmse_si_p_std = np.std(rmse_si_array)
    
    error_si.append(rmse_si_p_mean)
    error_svt.append(rmse_svt_p_mean)
    std_si.append(rmse_si_p_std)
    std_svt.append(rmse_svt_p_std)
    

fig = plt.figure()
plt.errorbar(prob, error_si, yerr = std_si, fmt='-o', markersize=4, capsize=4,color = 'blue', label = 'Soft-Impute');
plt.errorbar(prob, error_svt, yerr = std_svt, fmt='-o', markersize=4, capsize=4,color = 'red', label = 'SVT');