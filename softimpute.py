# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 23:19:14 2019
@title: Soft-impute algorithm
@author: Pablo
"""

import numpy as np
#import Matrix_creation as mc
import gaussian_creation as gc
import matplotlib.pyplot as plt
import time

def soft_impute(P_om_Z, mask, UV): 
    """Performs soft-impute algorithms for observation matrix P_om_Z,
    observed indices in mask.
    The function returns the average relative error between the estimated Z_hat 
    and original matrix UV"""
    
    error_pct = 0
    avg_time = 0
    
    m , n = len(P_om_Z), len(P_om_Z[0])

    Z_old = np.zeros((m,n))

    Z_hat_array = []

    P_om_Z_old = np.zeros((m,n))
    

    lam = np.linalg.norm(P_om_Z, 2)/1.5 #Largest singular value of matrix
    l = 0
    
    t_start = time.time()
    for l in range(2000):
        
            
        #Subspace projection by using mask matrix
        P_om_Z_old = mask*Z_old
        
        B = P_om_Z - P_om_Z_old + Z_old
        U,D,VT=(np.linalg.svd(B, full_matrices=False))
        
        D_thresh = np.maximum(D - lam, 0)
            
    
        Sigma = np.diag(D_thresh)
            # reconstruct matrix
        Z_hat = np.linalg.multi_dot([U, Sigma, VT])
        
        stopping_crit = (np.linalg.norm(Z_hat - Z_old, "fro")/np.linalg.norm(Z_hat, "fro"))
        if stopping_crit < 1e-6:
            break
        
        Z_hat_array.append(Z_hat)
        Z_old = Z_hat
        
        #Fractionally decreasing lambda
        lam = lam*((l+1)/(l+2))

    if Z_hat is not None:
        error_pct += (np.linalg.norm(Z_hat - UV, "fro")/np.linalg.norm(UV, "fro"))
        t_end = time.time()
        avg_time += t_end -t_start

    return Z_hat, avg_time, error_pct

