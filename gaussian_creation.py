# -*- coding: utf-8 -*-
"""
Created on Fri Nov  1 10:09:14 2019
@title: Gaussian creation
Description: Generates input Gaussian matrix
@author: Pablo
"""
import numpy as np

def noisy_gaussian(r, m=50, n=50, SNR=10):
    """Gaussian matrix of dimensions (m,n) corrupted with noise (SNR defined).
    For large SNR, effectively noiseless Gaussian matrix"""
    mu, sigma = 0, 1
    
    #Generate matrix Z with rank r, size m x n
    U = np.random.normal(mu, sigma, (m,r))
    V = np.random.normal(mu, sigma, (n,r))
    UV = np.matmul(U,V.transpose())
    
    Z = 0
    #Add noise
    mu_e = 0
    sigma_e = np.sqrt(np.var(UV))/SNR
    epsilon = np.random.normal(mu_e, sigma_e, (m,n))
    Z = UV + epsilon
    return UV, Z, sigma_e



def gaussian_remove_entries(A, p): 
    """Removes entries from Gaussian matrix A with probability p
    Missing entries are replaced with 0 (mean value)"""
    mask = np.random.binomial(1, (1-p), A.shape)                                
    B = mask*A
    indices = []
    values = []
    for i in range(len(A)):
        for j in range(len(A[0])):
            if mask[i][j] == 1:
                indices += i, j
                values += [A[i][j]]
                
    #Outputs incomplete matrix B, values and indices for observed entries
    return B, mask, indices, values    

