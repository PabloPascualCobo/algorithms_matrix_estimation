# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 11:08:53 2020
Applying matrix completion on Netflix data
@author: Pablo
Description: Use matrix completion algorithms to predict movie ratings 
from MovieLens 1M dataset, Section 2.6 of report.
Reference: https://www.kaggle.com/laowingkin/netflix-movie-recommendation
"""
import pandas as pd
import numpy as np
import SVT_algorithm_correct as sv
import softimpute as si
import gaussian_creation as gc
import matplotlib.pyplot as plt

def load_data():
    #Creates a 3-column user, movie, rating table
    '''load the Netflix dataset in a Pandas dataframe'''
    df = pd.read_csv('combined_data_1.txt', header=None, 
        names=['user_id', 'rating', 'timestamp'], 
        usecols=['user_id', 'rating'])
    df = df[:3000000]
    df_nan = pd.DataFrame(pd.isnull(df.rating))

    df_nan = df_nan[df_nan['rating'] == True]
    df_nan = df_nan.reset_index()

    
    movie_np = []
    movie_id = 1
    for i,j in zip(df_nan['index'][1:],df_nan['index'][:-1]):
        # numpy approach
        temp = np.full((1,i-j-1), movie_id)
        movie_np = np.append(movie_np, temp)
        movie_id += 1
    
    # Account for last record and corresponding length
    # numpy approach
    last_record = np.full((1,len(df) - df_nan.iloc[-1, 0] - 1),movie_id)
    movie_np = np.append(movie_np, last_record)
    
    
    # remove those Movie ID rows
    df = df[pd.notnull(df['rating'])]
    
    df['Movie_Id'] = movie_np.astype(int)
    df['user_id'] = df['user_id'].astype(int)
    
    #Return user-movie np array
    R_df = df.pivot(index = 'user_id', columns ='Movie_Id', values = 'rating')
    R_df = np.array(R_df)
    
    return R_df



prob = [0.1,0.2, 0.3, 0.4, 0.5, 0.6, 0.7]

t = 500 #Subsampling parameter

error_si = []
error_svt = []
std_si = []
std_svt = []

R = load_data()
print(R)

for i in prob:
    rmse_si_array = []
    rmse_svt_array = []
    p = i
    for j in range(10):
        np.random.seed(10+j)
        print(j)
        """Randomly subsample matrices- columns first then rows"""
        A = R[:,np.random.choice(R.shape[1], t, replace=False)]
        A = A[np.random.choice(A.shape[0], t, replace=False),:]
        A = A[:,:-1]
        A_base = np.nan_to_num(A)
        A0=np.nan_to_num(A)
        mask0 = np.nan_to_num(A0/A0)
        
        
        """Remove proportion p of entries"""
        A, mask1,_,_ = gc.gaussian_remove_entries(A0,p)
        mask2 = mask0*(1 - mask1) #Gives 1 in position of removed, known entries
        mask = mask0*mask1 #Remaining, known entries
        test_size = np.sum(mask2)
        real_p = test_size/(np.sum(mask0))
        
        """Calculate mean - done here as can only use available entries"""
        A[A==0]=np.nan
        mean_user_values = np.nan_to_num(np.array(np.nanmean(A, axis=1)).reshape(t,1))
        A_base = np.nan_to_num(A)
        A = A - mean_user_values
        A = np.nan_to_num(A)

        
        
        """Soft-impute"""
        B, time_run, _ = si.soft_impute(A, mask, A0)
        #B = np.zeros_like(A)
        
        """Post-processing - add back mean"""
        B = B + mean_user_values
        B = np.matrix.round(B)
        
        """Thresholds to ensure all predictions lie between 1 and 5"""
        B[B>5]=5
        B[B<1]=1
        
        """RMS Error"""
        rmse_si = (np.linalg.norm(mask2*(B - A0), "fro")/np.linalg.norm(mask2, "fro"))
        """Average Relative Error"""
        #rmse_si = (np.linalg.norm(mask2*(B - A0), "fro")/np.linalg.norm(mask2*A0, "fro"))
        
        print(rmse_si)
        
        rmse_si_array.append(rmse_si)
        
        """Run matrix completion algorithm"""
        """SVT"""
        B, time_run, _ = sv.SVT(A, mask, A0, p)
        #B = np.zeros_like(A)
        
        """Post-processing - add back mean"""
        B = np.matrix.round(B + mean_user_values)

        """Thresholds to ensure all predictions lie between 1 and 5"""
        B[B>5]=5
        B[B<1]=1

        """RMS error"""
        rmse_svt = (np.linalg.norm(mask2*(B - A0), "fro")/np.linalg.norm(mask2, "fro"))
        """Average Relative error"""
        #rmse_svt = (np.linalg.norm(mask2*(B - A0), "fro")/np.linalg.norm(mask2*A0, "fro"))

        print(rmse_svt)
        rmse_svt_array.append(rmse_svt)
    
    rmse_svt_p_mean = np.mean(rmse_svt_array)
    rmse_svt_p_std = np.std(rmse_svt_array)
    rmse_si_p_mean = np.mean(rmse_si_array)
    rmse_si_p_std = np.std(rmse_si_array)
    
    error_si.append(rmse_si_p_mean)
    error_svt.append(rmse_svt_p_mean)
    std_si.append(rmse_si_p_std)
    std_svt.append(rmse_svt_p_std)
    

fig = plt.figure()
plt.errorbar(prob, error_si, yerr = std_si, fmt='-o', markersize=4, capsize=4,color = 'blue', label = 'Soft-Impute');
plt.errorbar(prob, error_svt, yerr = std_svt, fmt='-o', markersize=4, capsize=4,color = 'red', label = 'SVT');
    