# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 16:22:07 2019
@title: SVT Algorithm
@author: Pablo
"""

import numpy as np
import time

def SVT(A, mask, UV, p): 
    """Performs SVT algorithms for input matrix A with proportion p 
    missing entries corresponding to indices in mask.
    Error measured with respect to full matrix UV"""
    """Returns estimate X, runtime and error"""
    
    #Set tuning parameters
    delta = 1.2/(1-p)
    k_0 = 1
        
    m , n = len(A), len(A[0])
    

    Y = k_0*delta*A
    
    
    tau = 5*n
    X = np.zeros_like(A)
    t_start = time.time()
    
    for j in range(2000):
        U,D,VT=(np.linalg.svd(Y, full_matrices=False))
        
        D_thresh = np.maximum(D - tau, 0)
            
    
        Sigma = np.diag(D_thresh)
        
        # reconstruct matrix
        X = np.linalg.multi_dot([U, Sigma, VT])
            

        stopping_crit = ((np.linalg.norm(mask*(X - A), "fro")/np.linalg.norm(A, "fro")))
        
        if stopping_crit < 1e-6:
            break
        
        
        Y = Y + delta*mask*(A- X)
        
    rel_error = (np.linalg.norm(X - UV, "fro")/np.linalg.norm(UV, "fro"))
    t_end = time.time()
    run_time = t_end -t_start
    return X, run_time, rel_error
