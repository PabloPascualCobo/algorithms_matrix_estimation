# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 21:46:29 2019
@title: cvxpy min NuclearNorm
Description: Performs matrix completion using cvxpy library,
constraints changed depending on noise or no noise
@author: Pablo
"""
import cvxpy as cp
import numpy as np
import Matrix_creation as mc
import gaussian_creation as gc
import time


def cvxp(A, mask, UV, p, sigma_e, indices, values): 
    """Performs norm minimisations of observation matrix A, 
    noise with std sigma_e, missing entries with probability p.
    The function returns the average relative error between the estimate
    and original matrix UV"""
    m , n = len(A), len(A[0])
    k= 0
    error_pct = 0
    avg_time = 0
    
    
    #If using entries 1-5, set pos=True
    #X = cp.Variable((m, n), pos=True)
    
    #If using Gaussian entries, set pos=False
    X = cp.Variable((m, n), pos=False)
    

    """
    #Objective and Constraints for NO NOISE
    constraints=[]
    
    objective = cp.normNuc(X)
    
    for i in range(len(values)):
        constraints += [X[indices[2*i], indices[2*i +1]]== values[i]]
    """    
    #Objective and Constraints WITH NOISE
    err_sum = 0
    for i in range (len(values)):
        err_sum += (values[i] - X[indices[2*i], indices[2*i +1]])**2
    lam = sigma_e*np.sqrt(2*(1-p)*n)
    #print(lam)

    objective = 0.5*err_sum + lam*cp.normNuc(X)
    #print(constraints)
    
    #print("Sparse matrix:\n", A1)
    t_start = time.time()
    problem = cp.Problem(cp.Minimize(objective))
    problem.solve()
    t_end = time.time()
    print(problem.solver_stats)
    A_hat = X.value
    #print((A_hat))
    if A_hat is not None:
        error_pct += (np.linalg.norm(A_hat - UV, "fro")/np.linalg.norm(UV, "fro"))
        k += 1
        avg_time += t_end -t_start
    #print("Estimate of A:\n",  A_hat)
        
        #print(error_pct)
    return avg_time, error_pct
