# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 10:19:12 2020
Description: Investigates the application of IMC for predicting gene-disease
associations, produces plots in Sec. 4.3.2, 4.3.3 of report
@author: Pablo
"""
import numpy as np
import sideinf_softimp as imc
import matplotlib.pyplot as plt
import dirty_imc

split1 = np.loadtxt("split1.txt", delimiter=',')
split2 = np.loadtxt("split2.txt", delimiter=',')
split3 = np.loadtxt("split3.txt", delimiter=',')
split = [split1, split2, split3, split1, split2]
GeneMicroarrayFeature = np.loadtxt("GeneMicroarrayFeatureNotCentredALT.txt", delimiter=',')
GeneHumanNetFeature = np.loadtxt("GeneHumanNetFeature.txt", delimiter=',')
GenePheneFeature = np.loadtxt("GenePheneFeature.txt", delimiter=',')

"""Gene Feature Matrix G"""
G = np.concatenate((GeneMicroarrayFeature, GeneHumanNetFeature, GenePheneFeature), axis = 1)


DiseaseSimilarityFeature = np.loadtxt("DiseaseSimilarityFeature.txt", delimiter=',')
DiseaseOMIMFeature = np.loadtxt("DiseaseOMIMFeatureNotCentredALT.txt", delimiter=',')

D = np.concatenate((DiseaseSimilarityFeature, DiseaseOMIMFeature), axis = 1)
"""Remove last 6 rows- correspond to new diseases (which aren't in training set)"""
D = D[:-6,:]


A_array = []
for j in range(len(split) - 2):
        print(j)
        M = split[j] + split[j+1]
        mask = np.nan_to_num(M/M)
        
        A_small = imc.sideinf_si(M, mask, M, G, D)[0]
        A_array.append(np.matmul(np.matmul(G, A_small),np.transpose(D)))
        
print(1)     
array_r = np.arange(5,250,10) #range of values of r tested
prob_array = []


#Evaluates matrix obtained by both algorithms
for r in array_r: 
    
    prediction_average = 0
    print(r)
    for j in range(len(A_array)):
        total_known = 0
        known_predicted = 0
        A = A_array[j]
        #Find probability that a true gene association is recovered in the top-r  
        #predictions.
        for col in range(len(A[0])):
            gene_list = A[:,col]
            topr_pred = np.argsort(gene_list)[-r:]
            #Find assciations for that disease from split 3
            geneassoc = np.nonzero(split[j+2][:,col])
            for i in geneassoc[0]:
                total_known +=1
                if i in topr_pred:
                    known_predicted += 1
        prediction_average += known_predicted/total_known * (1/3)          
    prob_array.append(prediction_average)
    

plt.plot(array_r, prob_array)