# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 11:26:14 2020
Inductive Matrix Completion (IMC) Algorithm
@author: Pablo
"""
import sideinf as sid
import numpy as np
import gaussian_creation as gc
import time



def sideinf_imc(M, mask, UV, A, B):
    """Performs IMC algorithm for input matrix M with side information 
    matrices A and B, missing entries with probability p.
    The function returns the average relative error between the estimated Z_hat 
    and original matrix UV
    
    Orthogonality of A and B is checked first to ensure the correct method is 
    used - avoid unnecessary computations of soft-impute"""
    
    t_start = time.time()
    
    orthogonal = False
    """Check orthogonality"""
    if np.linalg.norm(np.matmul(np.transpose(A),A) - np.identity(len(A[0])), 'fro')<1e-6:
        if np.linalg.norm(np.matmul(np.transpose(B),B) - np.identity(len(B[0])), 'fro')<1e-6:
            orthogonal = True
    
    error_pct = 0
    avg_time = 0
    
    r_a , r_b = len(A[0]), len(B[0])
    
    

    
    
    Z_old = np.zeros((r_a,r_b))
    #print(Z_old)
    
    Z_hat_array = []
    

    lam = np.linalg.norm(M, 2)/1.5 #Largest singular value of matrix
    l = 0
    
    prod_1 = np.matmul(np.matmul(A, Z_old),np.transpose(B))
    
    if orthogonal == False:
        A_pinv = np.linalg.pinv(A)
        B_pinv = np.linalg.pinv(B)
    
    
    
    for l in range(2000):
        
        #print(l)

            
        #Subspace projection by using mask matrix, prod_1 = AZoldB_T
        if orthogonal == False:
            update_mat = np.matmul(A_pinv,np.matmul(mask*(M - prod_1), np.transpose(B_pinv)))
        elif orthogonal == True:
            update_mat = np.matmul(np.transpose(A),np.matmul(mask*(M - prod_1), B))
        
        Y = Z_old + update_mat
        U,D,VT=(np.linalg.svd(Y, full_matrices=False))
        
        D_thresh = np.maximum(D - lam, 0)
            
    
        Sigma = np.diag(D_thresh)
            # reconstruct matrix
        Z_hat = np.linalg.multi_dot([U, Sigma, VT])
        
        stopping_crit = (np.linalg.norm(Z_hat - Z_old, "fro")/np.linalg.norm(Z_hat, "fro"))
        if stopping_crit < 1e-6:
            break
        
        Z_hat_array.append(Z_hat)
        Z_old = Z_hat
        lam = lam*((l+1)/(l+2))

        
        prod_1 = np.matmul(np.matmul(A, Z_old),np.transpose(B))
        
    if Z_hat is not None:
        error_pct += (np.linalg.norm(prod_1 - UV, "fro")/np.linalg.norm(UV, "fro"))
        t_end = time.time()
        avg_time += t_end -t_start

    return Z_hat, avg_time, error_pct

