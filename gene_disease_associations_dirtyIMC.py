 # -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 10:19:12 2020
Description: Investigates the application of IMC for predicting gene-disease
associations, produces plots in Sec. 4.3.4 of report
@author: Pablo
"""
import numpy as np
import sideinf_softimp as imc
import matplotlib.pyplot as plt
import dirty_imc

split1 = np.loadtxt("split1.txt", delimiter=',')
split2 = np.loadtxt("split2.txt", delimiter=',')
split3 = np.loadtxt("split3.txt", delimiter=',')

GeneMicroarrayFeature = np.loadtxt("GeneMicroarrayFeatureNotStandardisedALT.txt", delimiter=',')
GeneHumanNetFeature = np.loadtxt("GeneHumanNetFeature.txt", delimiter=',')
GenePheneFeature = np.loadtxt("GenePheneFeature.txt", delimiter=',')

"""Gene Feature Matrix G"""
G = np.concatenate((GeneMicroarrayFeature, GeneHumanNetFeature, GenePheneFeature), axis = 1)


DiseaseSimilarityFeature = np.loadtxt("DiseaseSimilarityFeature.txt", delimiter=',')
DiseaseOMIMFeature = np.loadtxt("DiseaseOMIMFeatureNotStandardisedALT.txt", delimiter=',')

D = np.concatenate((DiseaseSimilarityFeature, DiseaseOMIMFeature), axis = 1)
"""Remove last 6 rows- correspond to new diseases (which aren't in training set)"""
D = D[:-6,:]



#Reduce size of matrix + features to allow for performance in acceptable runtime,
#Take 1 of every 3 terms (reduced no. of columns, rows by 1/3)
split1_red = split1[0::3,0::3]
split2_red = split2[0::3,0::3]
split3_red = split3[0::3,0::3]
G_red = G[0::3,:]
D_red = D[0::3,:]

split = [split1_red, split2_red, split3_red, split1_red, split2_red]


A_array_IMC = []
A_array_dirty = []
for k in range(len(split) - 2):
        print(k)
        M = split[k] + split[k+1]
        mask = np.nan_to_num(M/M)
        
        #PLOT FOR DIRTY_IMC
        estim = dirty_imc.dirtyimc(M, mask, M, G_red, D_red)
        A_estim = estim[0]
        M_estim = estim[2]
        N_estim = estim[1]
        A_array_dirty.append(A_estim)
        
        #PLOT FOR IMC - BASELINE
        A_small = imc.sideinf_si(M, mask, M, G_red, D_red)[0]
        IMC_estim = np.matmul(np.matmul(G_red, A_small),np.transpose(D_red))
        A_array_IMC.append(IMC_estim)
        
print(1)     
array_r = np.arange(5,250,10)
prob_array_IMC = []
prob_array_dirty = []


#Evaluates matrix obtained by both algorithms
for r in array_r: 
    print(r)
    for j in range(len(A_array_IMC)):
        total_known = 0
        known_predicted_IMC = 0
        prediction_average_IMC = 0
        known_predicted_dirty = 0
        prediction_average_dirty = 0
        A = A_array_IMC[j]
        A_dirty = A_array_dirty[j]
        #Find probability that a true gene association is recovered in the top-r  
        #predictions.
        for col in range(len(A[0])):
            gene_list_IMC = A[:,col]
            topr_pred_IMC = np.argsort(gene_list_IMC)[-r:]
            gene_list_dirty = A_dirty[:,col]
            topr_pred_dirty = np.argsort(gene_list_dirty)[-r:]
            #Find assciations for that disease from split 3
            geneassoc = np.nonzero(split[j+2][:,col])
            for i in geneassoc[0]:
                total_known +=1
                if i in topr_pred_IMC:
                    known_predicted_IMC += 1
                if i in topr_pred_dirty:
                    known_predicted_dirty += 1
        prediction_average_IMC += known_predicted_IMC/total_known * (1/3)
        prediction_average_dirty += known_predicted_dirty/total_known * (1/3)       
    prob_array_IMC.append(prediction_average_IMC)
    prob_array_dirty.append(prediction_average_dirty)
    
plt.figure()
plt.plot(array_r, prob_array_IMC, color='black', label='IMC')
plt.plot(array_r, prob_array_dirty, color='green', label='Dirty IMC')