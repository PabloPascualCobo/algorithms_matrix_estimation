# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 00:31:10 2020
@title:Side Information Matrix Creation
@author: Pablo
Description: Generates orthogonal or non-orthogonal perfect side information matrices
"""
import numpy as np


def sideinf_mat(m, n, r_a, r_b, r):
    #Orthogonal feature matrices
    """Set r<=r_a<=m, r<=r_b<=n"""
    
    F = np.random.normal(0, 1, (m,n))
    
    U,D,VT=(np.linalg.svd(F, full_matrices=False))
    V = np.transpose(VT)
    
    """Get first r_a left singular vectors, first r_b right singular vectors,
    produce side information matrices A and B"""
    A = U[:,:r_a]
    B = V[:,:r_b]
    
    Z_a = np.random.normal(0,5,(r_a, r))
    Z_b = np.random.normal(0,5,(r_b, r))
    

    #Produce r_a x r_b matrix of rank r
    Z_0 = np.matmul(Z_a, np.transpose(Z_b))
    
    #Produce m x n matrix of rank r
    M = np.matmul(np.matmul(A, Z_0), np.transpose(B))
    
    return A, Z_0, B, M

def sideinf_mat_nonorth(m, n, r_a, r_b, r):
    """Set r<=r_a<=m, r<=r_b<=n"""
    
    """Create A and B from random normal distribution - non orthogonal"""
    A = np.random.normal(0,1,(m,r_a))
    B = np.random.normal(0,1,(n,r_b))
    
    #Normalise so each column has l2-norm of 1
    A = A/np.sqrt((A**2).sum(axis=0))
    B = B/np.sqrt((B**2).sum(axis=0))
    
    Z_a = np.random.normal(0,5,(r_a, r))
    Z_b = np.random.normal(0,5,(r_b, r))
    
    #Produce r_a x r_b matrix of rank r
    Z_0 = np.matmul(Z_a, np.transpose(Z_b))
    
    #Produce m x n matrix of rank r
    M = np.matmul(np.matmul(A, Z_0), np.transpose(B))
    
    return A, Z_0, B, M
