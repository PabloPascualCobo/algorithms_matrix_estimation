# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 20:56:01 2020
Plots for Performance of Side Information Matrices - section 3.2.1 of report
@author: Pablo
"""
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import numpy as np
#import cvxpy_ex as cvx
import softimpute as si
import gaussian_creation as gc
import SVT_algorithm_correct as sv
import imc
import sideinf as sid

n = 20 #n is number of different sampling probabilities
x = np.linspace(0.01, 0.8, n)

si_time_list = []
si_time = []
si_time_std = []
si_error_list = []
si_error = []
si_error_std = []

svt_time_list = []
svt_time = []
svt_time_std = []
svt_error_list = []
svt_error = []
svt_error_std = []
svt_time = []
svt_error = []

sid_time_list = []
sid_time = []
sid_time_std = []
sid_error_list = []
sid_error = []
sid_error_std = []


for j in range(n):
    print(j)
    si_time_list = []
    si_error_list = []
    svt_time_list = []
    svt_error_list = []
    cvx_time_list = []
    cvx_error_list = []
    for i in range(10):
        np.random.seed(i+150) #Set seed so same matrices are analysed for each p
        A,_,B,M = sid.sideinf_mat_nonorth(100, 100, 12, 8, 5)

        M_new, mask, _, _ = gc.gaussian_remove_entries(M, x[j])

        print(i)
        
        _, time, error =  imc.sideinf_imc(M_new, mask, M, A, B)
        sid_time_list.append(time)
        sid_error_list.append(error)
        
        _, time, error =  sv.SVT(M_new, mask, M, x[j])
        svt_time_list.append(time)
        svt_error_list.append(error)
        
        _, time, error =  si.soft_impute(M_new, mask, M)
        si_time_list.append(time)
        si_error_list.append(error)
        
        
    si_avg_time = np.mean(si_time_list)
    si_avg_time_std = np.std(si_time_list)
    si_avg_error = np.mean(si_error_list)
    si_avg_error_std = np.std(si_error_list)
    si_time.append(si_avg_time)
    si_error.append(si_avg_error)
    si_time_std.append(si_avg_time_std)
    si_error_std.append(si_avg_error_std)
    
    svt_avg_time = np.mean(svt_time_list)
    svt_avg_time_std = np.std(svt_time_list)
    svt_avg_error = np.mean(svt_error_list)
    svt_avg_error_std = np.std(svt_error_list)
    svt_time.append(svt_avg_time)
    svt_error.append(svt_avg_error)
    svt_time_std.append(svt_avg_time_std)
    svt_error_std.append(svt_avg_error_std)
    
    sid_avg_time = np.mean(sid_time_list)
    sid_avg_time_std = np.std(sid_time_list)
    sid_avg_error = np.mean(sid_error_list)
    sid_avg_error_std = np.std(sid_error_list)
    sid_time.append(sid_avg_time)
    sid_error.append(sid_avg_error)
    sid_time_std.append(sid_avg_time_std)
    sid_error_std.append(sid_avg_error_std)
    

plt.figure()
plt.errorbar(x, si_error, yerr = si_error_std, fmt='-o', markersize=4, capsize=4,color = 'blue', label = 'Soft-Impute');
plt.errorbar(x, svt_error, yerr = svt_error_std, fmt='-o', markersize=4, capsize=4,color = 'red', label = 'SVT'); 
plt.errorbar(x, sid_error, yerr = sid_error_std, fmt='-o', markersize=4, capsize=4,color = 'black', label = 'IMC');


plt.figure()
plt.errorbar(x, si_time,yerr = si_time_std, fmt='-o', markersize=4, capsize=4, color = 'blue', label = 'Soft-Impute');    
plt.errorbar(x, svt_time,yerr = svt_time_std, fmt='-o', markersize=4, capsize=4, color = 'red', label = 'SVT'); 
plt.errorbar(x, sid_time,yerr = sid_time_std, fmt='-o', markersize=4, capsize=4, color = 'black', label = 'IMC');
