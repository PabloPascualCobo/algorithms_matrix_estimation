# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 23:57:54 2019

@author: Pablo
@title: Matrix creation

"""

import numpy as np
from random import choices



def low_rank_matrix(A, k): #Takes input matrix A, output rank set to k
    U,D,VT=(np.linalg.svd(A))
    
    for i in range(len(D)):
        if i > k-1:
            D[i] = 0
    
    dim_sigma = min(A.shape[0], A.shape[1])
    Sigma = np.zeros((A.shape[0], A.shape[1]))
    # populate Sigma with n x n diagonal matrix
    Sigma[:dim_sigma, :dim_sigma] = np.diag(D)
    # reconstruct matrix
    B = U.dot(Sigma.dot(VT))
    return B #Outputs matrix B with rank k



def remove_entries(A, p): #Removes entries from matrix A with probability p
                            #Missing entries are replaced with 0
    B = np.zeros_like(A)
    mask = np.zeros_like(A)
    population = [1, 0]
    indices = []
    values =[]
    weights = [1-p, p]
    for i in range(len(A)):
        for j in range(len(A[0])):
            keep = (choices(population, weights))[0]
            if keep == 0:
                B[i][j] = 0
            else:
                B[i][j] = A[i][j]
                mask[i][j]=1
                indices += i, j
                values += [A[i][j]]

    return B, mask, indices, values #Returns matrix B with missing entries    
