# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 00:13:58 2020
Corruption of Side Information matrices with Noise
@author: Pablo
"""
import sideinf as sid
import numpy as np
import gaussian_creation as gc
import random

def noisy_feature_zeros(A, p):
    """NOT USED - Replaces feature columns with probability p by a column of 
    zeros - not used as caused problems for SVD"""
    orthogonal = False
    """Check orthogonality"""
    if np.linalg.norm(np.matmul(np.transpose(A),A) - np.identity(len(A[0])), 'fro')<1e-6:
        orthogonal = True
    mask = np.random.binomial(1, (1-p), len(A[0]))
    if orthogonal == False:
        A_pinv = np.linalg.pinv(A)
    for i in range(len(mask)):
        if mask[i] ==0:
            if orthogonal == True:
                A[:,i] = A[:,i] - np.matmul(A, np.matmul(np.transpose(A),A[:,i]))
            elif orthogonal == False:
                A[:,i] = A[:,i] - np.matmul(A, np.matmul(A_pinv,A[:,i]))
                
    return A

def noisy_feature(A, p):
    """METHOD USED- replace columns with probability p by component of random 
    vector orthogonal to side information matrix A"""
    orthogonal = False
    """Check orthogonality"""
    if np.linalg.norm(np.matmul(np.transpose(A),A) - np.identity(len(A[0])), 'fro')<1e-6:
        orthogonal = True
    
    """Choose columns to corrupt"""
    mask = np.random.binomial(1, (1-p), len(A[0]))
    if orthogonal == False:
        A_pinv = np.linalg.pinv(A)
    for i in range(len(mask)):
        if mask[i] ==0:
            x = np.random.normal(0,1,len(A[:,i]))
            if orthogonal == True:
                A[:,i] = x - np.matmul(A, np.matmul(np.transpose(A),x))
                A[:,i] = A[:,i]/np.linalg.norm(A[:,i],2)
            elif orthogonal == False:
                A[:,i] = x - np.matmul(A, np.matmul(A_pinv,x))
                A[:,i] = A[:,i]/np.linalg.norm(A[:,i],2)
    return A
